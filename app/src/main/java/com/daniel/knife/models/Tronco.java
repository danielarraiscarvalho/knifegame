package com.daniel.knife.models;

import com.daniel.knife.AndGraph.AGSprite;

import javax.microedition.khronos.opengles.GL10;

public class Tronco extends AGSprite {
    public Tronco(GL10 pOpenGL, int pCodImage, int pFrameWidth, int pFrameHeight, int pImageWidth, int pImageHeight) {
        super(pOpenGL, pCodImage, pFrameWidth, pFrameHeight, pImageWidth, pImageHeight);
    }

    public Tronco(GL10 pOpenGL, int pCodImage, int pTotalCol, int pTotalLin) {
        super(pOpenGL, pCodImage, pTotalCol, pTotalLin);
    }
}
