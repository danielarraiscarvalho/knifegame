package com.daniel.knife.cenas;

import com.daniel.knife.AndGraph.AGGameManager;
import com.daniel.knife.AndGraph.AGInputManager;
import com.daniel.knife.AndGraph.AGScene;
import com.daniel.knife.AndGraph.AGScreenManager;
import com.daniel.knife.AndGraph.AGSprite;
import com.daniel.knife.R;

public class Home extends AGScene {

    AGSprite play = null;
    AGSprite info = null;
    AGSprite conf = null;
    AGSprite logo = null;

    /*******************************************
     * Name: CAGScene()
     * Description: Scene construtor
     * Parameters: CAGameManager
     * Returns: none
     *****************************************
     * @param pManager*/
    public Home(AGGameManager pManager) {
        super(pManager);
    }

    @Override
    public void init() {
        //Chamada toda vez que a cena for ativada, exibida
        setSceneBackgroundColor(0.498f,0,0);

        //Adciona uma imagem a cena
        play = createSprite(R.mipmap.play, 1,1);
        play.setScreenPercent(20,20);
        play.vrPosition.setXY(AGScreenManager.iScreenWidth/2, (AGScreenManager.iScreenHeight/2));

        conf = createSprite(R.mipmap.conf, 1,1);
        conf.setScreenPercent(13,13);
        conf.vrPosition.setXY(AGScreenManager.iScreenWidth/2, (AGScreenManager.iScreenHeight/2)-370);

        info = createSprite(R.mipmap.exit, 1,1);
        info.setScreenPercent(13,13);
        info.vrPosition.setXY(AGScreenManager.iScreenWidth/2, (AGScreenManager.iScreenHeight/2)-680);

        logo = createSprite(R.mipmap.logo, 1,1);
        logo.setScreenPercent(46,12);
        logo.vrPosition.setXY(AGScreenManager.iScreenWidth/2, (AGScreenManager.iScreenHeight/2)+600);

        //Configura a Posição do play
    }

    @Override
    public void restart() {
        //Chamado após o retorno de uma interrupção
    }

    @Override
    public void stop() {
        //Chamado quando uma interrupção acontecer
    }

    @Override
    public void loop() {

        if(AGInputManager.vrTouchEvents.screenClicked()){
            if(play.collide(AGInputManager.vrTouchEvents.getLastPosition())){
                vrGameManager.setCurrentScene(2);
                return;
            }
            if(conf.collide(AGInputManager.vrTouchEvents.getLastPosition())){
                vrGameManager.setCurrentScene(2);
                return;
            }
            if(info.collide(AGInputManager.vrTouchEvents.getLastPosition())){
                vrGameManager.vrActivity.finish();
                return;
            }
        }
    }
}
